/*
 * Author: HuyTV
 */

#ifndef __PROJECT_SW_CONFIG_H__
#define __PROJECT_SW_CONFIG_H__

#include "stm32l4xx_hal.h"

#define TRACE_ENABLE 1

#if TRACE_ENABLE
	#define TRACE_ITM 1
	#define TRACE_UART 1
	#define TRACE_PRINTF 0
#endif

/* TRACE uart configuration */
#define TRACE_UART_CLK_ENABLE() 	__HAL_RCC_USART2_CLK_ENABLE()
#define TRACE_UART_GPIO_CLK_ENABLE() 	__HAL_RCC_GPIOA_CLK_ENABLE()
#define TRACE_UART_GPIO_PIN						(GPIO_PIN_2 | GPIO_PIN_3)
#define TRACE_UART_GPIO_PORT		GPIOA
#define TRACE_UART_GPIO_AF    GPIO_AF7_USART2
#define TRACE_UART_HANDLE       huart2
#define TRACE_UART_INSTANCE     USART2
#define TRACE_UART_AUTOBAUD     (0)
//#define TRACE_UART_IRQn         USART2_IRQn		// we will not use trace uart rx interrupt


#define TRACE_UART_BAUDRATE     (921600U)
#define TRACE_UART_HWFLOWCTRL   UART_HWCONTROL_NONE
#define TRACE_UART_WORDLENGTH   UART_WORDLENGTH_8B
#define TRACE_UART_STOPBITS     UART_STOPBITS_1
#define TRACE_UART_PARITY       UART_PARITY_NONE
#define TRACE_UART_MODE         UART_MODE_TX


/* MODEM configuration */
#define MODEM_UART_HANDLE       huart1
#define MODEM_UART_INSTANCE     USART1
#define MODEM_UART_AUTOBAUD     (0)
#define MODEM_UART_IRQn         USART1_IRQn


#define MODEM_UART_BAUDRATE     (115200U)
#define MODEM_UART_HWFLOWCTRL   UART_HWCONTROL_NONE
#define MODEM_UART_WORDLENGTH   UART_WORDLENGTH_8B
#define MODEM_UART_STOPBITS     UART_STOPBITS_1
#define MODEM_UART_PARITY       UART_PARITY_NONE
#define MODEM_UART_MODE         UART_MODE_TX_RX

/* ---- MODEM other pins configuration ---- */
#define MODEM_RST_GPIO_Port             GPIOB       /* MDM_RST_GPIO_Port */
#define MODEM_RST_Pin                   GPIO_PIN_4  /* MDM_RST_Pin */
#define MODEM_PWR_EN_GPIO_Port          GPIOB       /* MDM_PWR_EN_GPIO_Port */
#define MODEM_PWR_EN_Pin                GPIO_PIN_5  /* MDM_PWR_EN_Pin */

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;

#endif /* __PROJECT_SW_CONFIG_H__ */
