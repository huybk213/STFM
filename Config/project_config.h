/*
 * Author: HuyTV
 */

#ifndef __PROJECT_CONFIG_H__
#define __PROJECT_CONFIG_H__

#include "global_def.h"
#include "project_sw_config.h"
#include "project_hw_config.h"
#include "cmsis_os.h"

#endif /* __PROJECT_CONFIG_H__ */
