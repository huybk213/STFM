/*
 * Author: HuyTV
 */

#include "at_custom.h"
#include "at_core.h"
#include "trace.h"

const atcustom_LUT_t ATCMD_SIM900A_LUT[] =
{
	/* cmd enum		-			cmd string		-			Answer expect - 	cmd timeout (in ms)		-			send cmd ftion		-			analyze cmd error function */
	{  _AT_NULL,        	"",       							"OK",								AT_DEFAULT_TIMEOUT,  							fsendNoAtCmdNoParams,   			NULL						},
	{  _AT,          		"AT",       		 		"OK",						AT_DEFAULT_TIMEOUT,  				fsendAtCmdNoParams,   			fRspAnalyzeNone	},
	{  _AT_CMGF_1,      "AT+CMGF=1",     		"OK",						AT_DEFAULT_TIMEOUT,  				fsendAtCmdNoParams,   			fRspAnalyzeNone	},
	{ _AT_CSCS_GSM,		"AT+CSCS=\"GSM\"", 		"OK", 					AT_DEFAULT_TIMEOUT, 				fsendAtCmdNoParams, 					fRspAnalyzeNone	},
	{ _AT_CMGS,				"AT+CMGS=", 					">", 					AT_DEFAULT_TIMEOUT, 					fsendAtCmdWithParams, 				fRspAnalyzeNone	},
	{ _AT_GET_CMGS,		"", 								"+CMGS: ", 					AT_DEFAULT_TIMEOUT, 			fsendAtCmdNoParams, 				fRspAnalyzeNone	},
};

uint16_t size_ATCMD_SIM900A_LUT  = ((uint16_t) (sizeof (ATCMD_SIM900A_LUT) / sizeof (atcustom_LUT_t)));

/* This function get command to be send to modem in Look up table */
const uint8_t* getCMDStringInLUT(at_context* at_context, uint16_t cmd_id)
{
	if(cmd_id >= at_context->modem_LUT_size)
	{
		LOG("cmd_id out of range\r\n");
		goto exit;
	}
	
	for(uint16_t i = 0; i < at_context->modem_LUT_size; i++)
	{
		if(at_context->p_modem_LUT[i].cmd_id != cmd_id)
		{
		}
		else
		{
			return (const uint8_t*)at_context->p_modem_LUT[i].cmd_name;
		}
	}
	exit:
	LOG("<%s, %d> getCMDStringInLUT not found, cmd_id = %d\r\n", __FILE__, __LINE__, cmd_id);
	return (uint8_t*)"";		// return default value, just dont care
}

uint16_t getIndexInLUTByCMDId(at_context* at_context, uint16_t cmd_id)
{
	for(uint16_t i = 0; i < at_context->modem_LUT_size; i++)
	{
		if(at_context->p_modem_LUT[i].cmd_id != cmd_id)
		{
			i++;
		}
		else
		{
			return cmd_id;
		}
	}
	return AT_CMD_INVALID;		// meaning no cmd id found in LUT 
}

at_result_t fGetMessage(at_context *at_context)
{
		at_result_t retval = AT_RES_ERROR;
		uint16_t lut_index  = at_context->cmd_context.at_cmd;			// check context here
		uint32_t timeout  = at_context->p_modem_LUT[lut_index].response_timeout;
		uint32_t start_tick = xTaskGetTickCount();
		osEvent evt;
		LOG("Trial step = %d\r\n", at_context->trial_step);
		while(((xTaskGetTickCount() - start_tick) < timeout))   // wait for message)
		{
			do
			{
				evt = osMailGet(pool_sim_mail_id, 1);
				tPostSimMail_t *tmp_mail;
				if(evt.status == osEventMail)
				{
					memset(tmp_mail->msg, 0, tmp_mail->size);
					tmp_mail = (tPostSimMail_t*)evt.value.p;
					LOG("Got message: ");
					LOG_PRINTF(tmp_mail->msg, tmp_mail->size);
					LOG(". ");
					LOG("We want: \"%s\". ", (char*)at_context->p_modem_LUT[lut_index].answer_expected);
					// LOG("Answer expect: %s, length: %d\r\n", (char*)at_context->p_modem_LUT[lut_index].answer_expected, strlen((char*)at_context->p_modem_LUT[lut_index].answer_expected));
					if(memcmp((char*)at_context->p_modem_LUT[lut_index].answer_expected, (char*)tmp_mail->msg, strlen((char*)at_context->p_modem_LUT[lut_index].answer_expected)) == 0)
					{
						LOG("Expected answer\r\n");
						retval = AT_RES_OK;
						if(osMailFree(pool_sim_mail_id, tmp_mail) != osOK)
						{
							LOG("osMailFree return error\r\n");
							Error_Handler();
						}
						goto exit;
					}
					else
					{
						LOG("Unexpected answer\r\n");
						memcpy(at_context->error_message.msg, tmp_mail->msg, tmp_mail->size);
						at_context->error_message.size = tmp_mail->size;
						retval = AT_RES_ERROR;
						if(osMailFree(pool_sim_mail_id, tmp_mail) != osOK)
						{
							LOG("osMailFree return error\r\n");
							Error_Handler();
						}
						 at_context->crlf_state = WAIT_FOR_FIRST_CR;
						// goto exit;
					}
				}
			} while(at_context->trial_step--);
			
		}
		retval = AT_RES_TIMEOUT_WITH_NO_MESSAGE;
		exit:
		return retval;
}

tPostSimMail_t tmp_expected_message;
uint8_t tmp_get_echo_testing[128];
uint8_t tmp_get_echo_index = 0;

at_result_t fGetEchoMessage(at_context *at_context, uint8_t* expected_message, uint16_t length)
{
	at_result_t retval = AT_RES_ERROR;
	memset(tmp_expected_message.msg, sizeof(tmp_expected_message.msg)/ sizeof(uint8_t), 0);
	tmp_expected_message.size = 0;
	if(at_context->at_mode != AT_MODE_ECHO)
	{
		retval = AT_RES_INVALID_PARAMS;
		LOG("at_context->at_mode is not AT_MODE_ECHO\r\n");
	}
	
		osEvent evt;
		uint32_t start_tick = xTaskGetTickCount();
		while(((xTaskGetTickCount() - start_tick) < AT_ECHO_TIMEOUT))   // wait for message)
		{
			evt = osMailGet(pool_sim_mail_id, 2);
			tPostSimMail_t *tmp_mail;
			if(evt.status == osEventMail)
			{
				tmp_mail = (tPostSimMail_t*)evt.value.p;
				uint8_t ii = tmp_mail->size;
				LOG("Get echo: %d, size: %d. ", tmp_mail->msg[0], ii);
				for(uint16_t i = 0; i < ii; i++)
				{
					tmp_expected_message.msg[tmp_expected_message.size] = tmp_mail->msg[i];
					tmp_expected_message.size++;
					tmp_get_echo_testing[tmp_get_echo_index%128] = tmp_mail->msg[i];
					tmp_get_echo_index++;
				}
				if(osMailFree(pool_sim_mail_id, tmp_mail) != osOK)
				{
					Error_Handler();
				}
				if(strcmp((char*)expected_message, (char*)tmp_expected_message.msg) == 0)
				{
					LOG("Expected echo\r\n");
					retval = AT_RES_OK;
					goto exit;
				}
				else
				{
					
				}
			}
		}
	
	exit:
	LOG("Expected message: %s\r\n", (char*)expected_message);
	LOG("We got: %s\r\n",(char*)tmp_expected_message.msg);
	memset(tmp_expected_message.msg, 0, tmp_expected_message.size);
	tmp_expected_message.size = 0;
	return retval;
	
}


at_result_t fsendAtCmdNoParams(at_context *at_context, uint16_t cmd_id, uint8_t* param, uint16_t length_param)		
{
  /*UNUSED(p_atp_ctxt);*/
  /*UNUSED(p_modem_ctxt);*/
	/* Param is NULL, length_pram is 0 */
	at_result_t retval = AT_RES_ERROR;
	const uint8_t* raw_cmd;
	if(at_context == NULL)
	{
		retval = AT_RES_INVALID_PARAMS;
	}
	
	if(at_context->cmd_context.cmd_state != CMD_STATE_IDLE)
	{
		LOG("Error, invalid cmd state, another process currently sending command\r\n");
		retval = AT_RES_INVALID_ELEMENT_STATE;
		goto exit;
	}
	
	if(at_context->crlf_state != WAIT_FOR_FIRST_CR)
	{
		LOG("Error, CRLF state invalid\r\n");
		retval = AT_RES_INVALID_ELEMENT_STATE;
		goto exit;
	}
	
	at_context->cmd_context.cmd_state = CMD_STATE_PROCESSING;
	at_context->cmd_context.at_cmd = cmd_id;
//	LOG("at_context->cmd_context.at_cmd = %d\r\n", at_context->cmd_context.at_cmd);
	raw_cmd = getCMDStringInLUT(at_context, cmd_id);
	
	uartSendBuffer(&MODEM_UART_HANDLE, (uint8_t*)raw_cmd, strlen((char*)raw_cmd));
	uartSendBuffer(&MODEM_UART_HANDLE, (uint8_t*)"\r\n", strlen((char*)"\r\n"));
	
	at_context->cmd_context.cmd_state = CMD_STATE_IDLE;
	retval = AT_RES_OK;
	
	exit:
  return retval;
}

at_result_t fsendNoAtCmdNoParams(at_context *at_context, uint16_t cmd_id, uint8_t* param, uint16_t length_param)	
{
	at_result_t retval = AT_RES_OK;
	const uint8_t* raw_cmd;
	if(at_context == NULL)
	{
		retval = AT_RES_INVALID_PARAMS;
		LOG("fsendNoAtCmdNoParams return AT_RES_INVALID_PARAMS\r\n");
	}
	
	if(at_context->cmd_context.cmd_state != CMD_STATE_IDLE)
	{
		LOG("Error, invalid cmd state, another process currently sending command\r\n");
		retval = AT_RES_INVALID_ELEMENT_STATE;
		goto exit;
	}
	
	if(at_context->crlf_state != WAIT_FOR_FIRST_CR)
	{
		LOG("Error, CRLF state invalid\r\n");
		retval = AT_RES_INVALID_ELEMENT_STATE;
		goto exit;
	}
	at_context->cmd_context.at_cmd = cmd_id;
	exit:
	return retval;
}

at_result_t fsendAtCmdWithParams(at_context *at_context, uint16_t cmd_id, uint8_t* param, uint16_t param_length)		
{
  /*UNUSED(p_atp_ctxt);*/
  /*UNUSED(p_modem_ctxt);*/
	/* Param is NULL, length_pram is 0 */
	at_result_t retval = AT_RES_ERROR;
	const uint8_t* raw_cmd;
	if(at_context == NULL)
	{
		retval = AT_RES_INVALID_PARAMS;
	}
	
	if(at_context->cmd_context.cmd_state != CMD_STATE_IDLE)
	{
		LOG("Error, invalid cmd state, another process currently sending command\r\n");
		retval = AT_RES_INVALID_ELEMENT_STATE;
		goto exit;
	}
	
	if(at_context->at_mode == AT_MODE_CMD)
	{
		if(at_context->crlf_state != WAIT_FOR_FIRST_CR)
		{
			LOG("Error, CRLF state invalid\r\n");
			retval = AT_RES_INVALID_ELEMENT_STATE;
			goto exit;
		}
	}
	at_context->cmd_context.cmd_state = CMD_STATE_PROCESSING;
	at_context->cmd_context.at_cmd = cmd_id;
// 	LOG("at_context->cmd_context.at_cmd = %d\r\n", at_context->cmd_context.at_cmd);
	raw_cmd = getCMDStringInLUT(at_context, cmd_id);
	
	uartSendBuffer(&MODEM_UART_HANDLE, (uint8_t*)raw_cmd, strlen((char*)raw_cmd));
	uartSendBuffer(&MODEM_UART_HANDLE, (uint8_t*)param, param_length);
	uartSendBuffer(&MODEM_UART_HANDLE, (uint8_t*)"\r\n", strlen((char*)"\r\n"));
	
	at_context->cmd_context.cmd_state = CMD_STATE_IDLE;
	retval = AT_RES_OK;
	
	exit:
  return retval;
}


at_action_t fRspAnalyzeNone(at_context *at_context)
{
	at_action_t retval = ACTION_DO_NOTHING;
	if(at_context == NULL)
	{
		LOG("<%s, %d> parameter is null\r\n", __FILE__, __LINE__);
		retval = ACTION_DO_NOTHING;
		goto exit;
	}
	
	if(strcmp((const char*)at_context->error_message.msg, "") == 0)
	{
		retval = ACTION_DO_NOTHING;
		LOG("<%s, %d> Error message is null\r\n", __FILE__, __LINE__);
		goto exit;
	}
	
	// all error must hanle here
	if(strcmp((const char*)at_context->error_message.msg, "ERROR") == 0)
	{
		LOG("<%s, %d>, Detected error message\r\n", __FILE__, __LINE__);
		retval = ACTION_DO_NOTHING;
		goto exit;
	}
	else
	{
		LOG("<%s, %d> Unhandle error message\r\n", __FILE__, __LINE__);
		goto exit;
	}
		
	// 
	
//	uint16_t lut_index  = at_context->current_cmd;
	// uint32_t timeout  = at_context->p_modem_LUT[lut_index].response_timeout;
	
	exit:
	/* Clear error message */
	memset(at_context->error_message.msg, 0, at_context->error_message.size);
	at_context->error_message.size = 0;
	
	return (retval);
}
