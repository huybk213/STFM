/*
 * Author: HuyTV
 */ 

#ifndef __AT_CONTEXT_H__
#define __AT_CONTEXT_H__

#include "at_define.h"
#include "circular.h"

#define MAX_ERROR_MESSAGE_SIZE 128

typedef struct
{
	uint8_t msg[MAX_ERROR_MESSAGE_SIZE];
	uint16_t size;
} at_current_error_message_t;

typedef struct
{
	at_standart_cmd_t at_cmd;
	cmd_state_t cmd_state;
} at_cmd_context;

typedef struct at_context_t
{
	uint16_t                       			modem_LUT_size;
  const struct atcustom_LUT_struct   *p_modem_LUT;
	at_cmd_context 											cmd_context;
	CRLF_state_t 												crlf_state;
	at_current_error_message_t				  error_message;
	at_mode_t 													at_mode;
	uint8_t 														trial_step;	
} at_context;

#endif /* __AT_CONTEXT_H__ */
