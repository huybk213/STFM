/*
 * Author: HuyTV
 */ 

#ifndef __AT_CORE_H__
#define __AT_CORE_H__

#include "at_define.h"
#include "at_context.h"
#include "usart.h"
#include "project_config.h"

extern at_context cellular_context;
extern osMailQId  pool_sim_mail_id;
extern at_context atcore_context;

/* Common functions */
void atcoreResetContext(at_context *at_context); 		// Reset init state for at context
void atcoreInitContext(at_context *at_context);
void atcoreProcessData(void);
bool atcoreSendUart(uint8_t* data, uint16_t length);
void atcoreInit(at_context *at_context);

/* Basic AT functions */
at_result_t checkATResponse(void);

/* SMS function */
at_result_t atSendSMS(uint8_t* number, uint8_t* message, uint16_t message_length, at_context* atcore_context);

#endif /* __AT_CORE_H__ */
