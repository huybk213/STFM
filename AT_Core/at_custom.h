/*
 * Author: HuyTV
 */ 

#ifndef __AT_CUSTOM_H__
#define __AT_CUSTOM_H__

#include "at_define.h"
#include "at_context.h"


#define CMD_AT_NAME_MAX_LENGTH 32
#define CMD_AT_ANSWER_EXPECT_MAX_LENGTH 128
#define AT_DEFAULT_TIMEOUT       ((uint32_t)10000)  /* timeout(ms) for AT */
#define AT_ECHO_TIMEOUT						(uint32_t)10000		/* timeout(ms) in echo mode */
#define AT_CMD_INVALID ((uint16_t)0xFFFF)


typedef at_result_t (*atCMDFunctionTypeDef)(at_context *at_context, uint16_t id, uint8_t* param, uint16_t length_param);
typedef at_action_t (*analyzeFunctionTypeDef)(at_context *at_context);

struct atcustom_LUT_struct;
typedef struct atcustom_LUT_struct
{
	uint32_t					cmd_id;
	uint8_t						cmd_name[CMD_AT_NAME_MAX_LENGTH];		// cmd_name meaning cmd will send and received, ex send "AT\r\n", sim will answer AT\r\nOK\r\n, AT is cmd name
	uint8_t						answer_expected[CMD_AT_ANSWER_EXPECT_MAX_LENGTH];	
	uint32_t					response_timeout;
	atCMDFunctionTypeDef       	atCMDFunction;
	analyzeFunctionTypeDef 		analyzeFunction;
} atcustom_LUT_t;

extern const atcustom_LUT_t ATCMD_SIM900A_LUT[];
extern uint16_t size_ATCMD_SIM900A_LUT;


at_result_t fsendAtCmdNoParams(at_context *at_context, uint16_t id, uint8_t* param, uint16_t length_param);
at_result_t fsendAtCmdWithParams(at_context *at_context, uint16_t cmd_id, uint8_t* param, uint16_t length_param);		
at_result_t fsendNoAtCmdNoParams(at_context *at_context, uint16_t id, uint8_t* param, uint16_t length_param);

at_action_t fRspAnalyzeNone(at_context *at_context);
uint16_t atGetIndexInLUT(at_context *at_context, uint16_t id);
const uint8_t* getCMDStringInLUT(at_context* at_context, uint16_t cmd_id);
uint16_t getIndexInLUTByCMDId(at_context* at_context, uint16_t cmd_id);
at_result_t fGetMessage(at_context *at_context);
at_result_t fGetEchoMessage(at_context *at_context, uint8_t* expected_message, uint16_t length);

#endif /* __AT_CUSTOM_H__ */
