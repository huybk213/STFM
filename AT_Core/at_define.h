/*
 * Author: HuyTV
 */

#ifndef __AT_DEFINE_H__
#define __AT_DEFINE_H__

#include <stdint.h>
#include <stdbool.h>

typedef enum 
{
	AT_RES_OK = 0,
	AT_RES_ERROR,
	AT_RES_TIMEOUT_WITH_NO_MESSAGE,
	AT_RES_INVALID_PARAMS,
	AT_RES_INVALID_ELEMENT_STATE, 
	AT_RES_UNKNOWN_ERROR
} at_result_t;

typedef enum 
{
    AT_SIM_NOT_INSERTED = 0,
    AT_SIM_INSERTED
} at_simcard_state_t;

typedef enum
{
	STARTING = 0,
	READY
} at_sim_ready_t;

typedef enum
{
    AT_SIM1 = 1,
    AT_SIM2
} at_sim_slot_t ;

typedef enum
{
		CMD_STATE_IDLE = 0,		// free
    CMD_STATE_PROCESSING			// currently sending cmd
} cmd_state_t;

typedef enum
{
//    WAIT_FOR_FIRST_CR = 0,
    WAIT_FOR_FIRST_CR,
    WAIT_FOR_FIRST_LF,
		WAIT_FOR_MSG,
//    WAIT_FOR_LAST_CR,
    WAIT_FOR_LAST_LF_END
} CRLF_state_t;

typedef enum
{
	ACTION_DO_NOTHING = 0,
	ACTION_DO_PROCESSING_SMS_INCOMMING,
	ACTION_DO_PROCESSING_CALL_INCOMMING,
	ACTION_DO_GET_NETWORK_DATA,
} at_action_t;

typedef enum
{
	_AT_NULL = (uint16_t)0,
	_AT = (uint16_t)1,								/* Normal AT command	*/
	_AT_CMGF_1 = (uint16_t)2,					/* Select message format is text	*/
	_AT_CSCS_GSM = (uint16_t)3,
	_AT_CMGS = (uint16_t)4,						/*  Send text message */
	_AT_GET_CMGS = (uint16_t)5				/* Get +CMGS: xx */
} at_standart_cmd_t;

typedef enum 
{
	AT_MODE_CMD  = (uint8_t)0,
	AT_MODE_DATA = (uint8_t)1,
	AT_MODE_ECHO = (uint8_t)2
} at_mode_t;

// #define __AT    "AT"
// #define __ATZ   "ATZ"
// #define __CTRLZ (0x1A)
// #define __RING  "RING"


#endif /* __AT_DEFINE_H__ */
