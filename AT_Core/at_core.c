/*
 * Author: HuyTV
 */

#include "at_core.h"
#include "trace.h"
#include "sysctrl.h"
#include "main.h"
#include "global_def.h"
#include "at_custom.h"

osMailQDef(pool_sim_mail_id, 8, tPostSimMail_t);                    // Define mail queue
osMailQId  pool_sim_mail_id;

at_context atcore_context;

void atcoreResetContext(at_context *at_context)
{
		at_context->cmd_context.at_cmd = _AT_NULL;
		at_context->crlf_state = WAIT_FOR_FIRST_CR;
		at_context->cmd_context.cmd_state = CMD_STATE_IDLE;
		at_context->p_modem_LUT = (const atcustom_LUT_t *)ATCMD_SIM900A_LUT;
		at_context->modem_LUT_size = size_ATCMD_SIM900A_LUT;
		at_context->error_message.msg[0] = '\0';
		at_context->error_message.size = 0;
		at_context->at_mode = AT_MODE_CMD;
		at_context->trial_step = 0;
		// implement later
}

void atcoreInitContext(at_context *at_context)
{
	atcoreResetContext(at_context);
}

void atcoreOpen(at_context *at_context)
{
	// reset contex
	if(!SysCtrl_getDeviceDescriptor())	// config uart, sim power pin
	{
		LOG("SysCtrl_getDeviceDescriptor return error\r\n");
		Error_Handler();
	}
}

void atcoreInit(at_context *at_context)
{
		 pool_sim_mail_id = osMailCreate(osMailQ(pool_sim_mail_id), NULL);  /* create mail queue  */
		 if(pool_sim_mail_id == NULL)
		 {
			 LOG("osMailCreate creation return NULL pointer\r\n");
			 Error_Handler();
		 }
		atcoreInitContext(at_context); // reset context
		atcoreOpen(at_context);
		LOG("atcoreInit DONE\r\n");
}

void atcoreProcessData()
{
	
}

void atGetMessage()
{
	
}

bool atcoreSendUart(uint8_t* data, uint16_t length)
{
	return uartSendBuffer(&MODEM_UART_HANDLE, data, length);
}

at_result_t atSendSMS(uint8_t* number, uint8_t* message, uint16_t message_length, at_context* atcore_context)
{
	at_result_t retval = AT_RES_ERROR;
	uint8_t tmp_number[32];
	memset(tmp_number, 0, 32);
	if((number == NULL) || (message == NULL))
	{
		LOG_INVALID_PARAM();
		retval = AT_RES_INVALID_PARAMS;
		goto exit;
	}
	LOG("AT\r\n");
	if(checkATResponse() != AT_RES_OK)
	{
		LOG("Modem did not response AT command, check your wire connection\r\n");
		retval = AT_RES_ERROR;
		goto exit;
	}
	osDelay(20);
	/* Set SMS system into text mode */ 
	LOG("AT+CMGF=1\r\n");
	atcore_context->at_mode = AT_MODE_CMD;
	retval = fsendAtCmdNoParams(atcore_context , _AT_CMGF_1, NULL, 0);			
	if(retval != AT_RES_OK)
	{
		retval = AT_RES_ERROR;		// Check sim card insert status
		goto exit;
	}
	retval = fGetMessage(atcore_context);
	if(retval != AT_RES_OK)
	{
		retval = AT_RES_ERROR;
		goto exit;
	}
	osDelay(20);
	LOG("AT+CSCS=\"GSM\"\r\n");
	retval = fsendAtCmdNoParams(atcore_context , _AT_CSCS_GSM, NULL, 0);			
	if(retval != AT_RES_OK)
	{
		retval = AT_RES_ERROR;
		goto exit;
	}
	retval = fGetMessage(atcore_context);
	if(retval != AT_RES_OK)
	{
		retval = AT_RES_ERROR;
		goto exit;
	}
	
	// AT+CMGS="phone number"<CR><LF>
	LOG("AT send message\r\n");
	atcore_context->at_mode = AT_MODE_DATA;
	strcat((char*)tmp_number, "\"");
	strcat((char*)tmp_number, (char*)number);
	strcat((char*)tmp_number, "\"");
	retval = fsendAtCmdWithParams(atcore_context , _AT_CMGS, tmp_number, strlen((char*)tmp_number));			
	if(retval != AT_RES_OK)
	{
		retval = AT_RES_ERROR;
		goto exit;
	}
	retval = fGetMessage(atcore_context);
	if(retval != AT_RES_OK)
	{
		retval = AT_RES_ERROR;
		goto exit;
	}
	atcore_context->at_mode = AT_MODE_ECHO;
	if(!atcoreSendUart(message, message_length))
	{
		LOG("AT send SMS message error\r\n");
		retval = AT_RES_ERROR;
		goto exit;
	}
	
	retval = fGetEchoMessage(atcore_context, message, message_length);
	if(retval)
	{
			LOG("AT send SMS message error in function fGetEchoMessage \r\n");
			retval = AT_RES_ERROR;
			goto exit;
	}
	uartSendByte(&MODEM_UART_HANDLE, 0x1A); 		// send ctrlZ to end message
	atcore_context->at_mode = AT_MODE_CMD;
	// we will get >{0D}{0A}+CMGS: 37{0D}{0A}{0D}{0A}OK{0D}{0A}
	// not done
	retval = fsendNoAtCmdNoParams(atcore_context , _AT_NULL, NULL, 0);				// we didnot send anything	
	if(retval != AT_RES_OK)
	{
		retval = AT_RES_ERROR;
		goto exit;
	}
		
	atcore_context->trial_step = 1;			// first is +CMGS, second is OK
	retval = fGetMessage(atcore_context);
	if(retval != AT_RES_OK)
	{
		retval = AT_RES_ERROR;
		LOG("fGetMessage error\r\n");
		goto exit;
	}
	
	LOG("Send message success\r\n");
	exit:
	atcore_context->at_mode = AT_MODE_CMD;
	atcore_context->trial_step = 0;
	atcore_context->crlf_state = WAIT_FOR_FIRST_CR;
	// reset context
	return retval;
}

at_result_t checkATResponse(void)
{
			at_result_t retval;
			LOG("<%s, %d> _AT: %d\r\n", __FILE__, __LINE__, _AT);
			retval = fsendAtCmdNoParams(&atcore_context , _AT, NULL, 0);
			if(retval == AT_RES_OK)
			{				
							retval = fGetMessage(&atcore_context);
							if( retval != AT_RES_OK)
							{
//								fRspAnalyzeNone(&atcore_context);
								retval = AT_RES_ERROR;
							}
			}
			else retval = AT_RES_ERROR;
			
			return retval;
}

