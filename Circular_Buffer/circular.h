/*
 * Author: HuyTV
 */

#ifndef __CIRCULAR_H__
#define __CIRCULAR_H__

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#define RING_BIGGEST_SIZE 256
#define MAXIMUM_MESSAGE 12
#define MAXIMUM_MESSAGE_LENGTH 64

typedef struct
{
  uint8_t buffer[RING_BIGGEST_SIZE];
  uint16_t wrIdx;      // write index
  uint16_t rdIdx;      // read index 
  bool isBusy;
} ring_buffer_t;

static inline bool ringBufferIsEmpty(ring_buffer_t* data)
{
  if(data->wrIdx != data->rdIdx)
  {
    return  false;
  }
  return true;
}

static inline bool ringBufferIsFull(ring_buffer_t* data)
{
  if(data->wrIdx != ((data->rdIdx + RING_BIGGEST_SIZE) & 0x0FFFF))
  {
    return  false;
  }
  return true;
}

// write fifo data
static inline bool ringBufferWrite(ring_buffer_t* data, uint8_t wr_data)
{
  if(data->isBusy)
  {
    return false;
  }
  if(ringBufferIsFull(data))
  {
    return false;
  }
  data->isBusy = true;
  uint8_t current_write_index = (RING_BIGGEST_SIZE-1) & data->wrIdx;
 
  data->buffer[current_write_index] =  wr_data;
  
	data->wrIdx++;
  data->isBusy = false;
	
  return true;
}

static inline bool ringBufferRead(ring_buffer_t* data, uint8_t* rd_data)      // read fifo data
{
  if(ringBufferIsEmpty(data))
  {
    return false;
  }
  data->isBusy = true;
  uint8_t current_read_index  = (RING_BIGGEST_SIZE-1) & data->rdIdx; 
	
	*rd_data = data->buffer[current_read_index];

  data->rdIdx++;
  data->isBusy = false;
	
  return true;
}

static inline void ringBufferReset(ring_buffer_t* data)    // reset fifo
{
  data->wrIdx = data->rdIdx = 0;
}


typedef struct
{
	uint8_t data[MAXIMUM_MESSAGE_LENGTH];
	uint16_t length;
}	gsm_response_t;

//typedef struct
//{
//  gsm_response_t gsm_response[MAXIMUM_MESSAGE];			// like AT, RING, ERROR
//  uint16_t wrIdx;      // write index
//  uint16_t rdIdx;      // read index 
//  bool isBusy;
//} circular_gsm_response_t;

//static inline bool circularGsmMessageIsEmpty(circular_gsm_response_t* data)
//{
//  if(data->wrIdx != data->rdIdx)
//  {
//    return  false;
//  }
//  return true;
//}

//static inline bool circularGsmMessageIsFull(circular_gsm_response_t* data)
//{
//	if(data->wrIdx != ((data->rdIdx + RING_BIGGEST_SIZE) & 0x0FFFF))
//  {
//    return  false;
//  }
//  return true;
//}

//static inline bool  circularGsmMessageWrite(circular_gsm_response_t* data, gsm_response_t* wr_data)
//{
//	if(data->isBusy)
//  {
//    return false;
//  }
//  if(circularGsmMessageIsFull(data))
//  {
//    return false;
//  }
//  data->isBusy = true;
//  uint8_t current_write_index = (MAXIMUM_MESSAGE-1) & data->wrIdx;
// 
//  memcpy(data->gsm_response->data, wr_data->data, wr_data->length);
//  data->gsm_response->length = wr_data->length;
//	
//	data->wrIdx++;
//  data->isBusy = false;
//	
//  return true;
//}


//static inline bool circularGsmMessageRead(circular_gsm_response_t* data, gsm_response_t* rd_data)      // read fifo data
//{
//  if(circularGsmMessageIsEmpty(data))
//  {
//    return false;
//  }
//  data->isBusy = true;
//  uint8_t current_read_index  = (MAXIMUM_MESSAGE-1) & data->rdIdx; 
//	
//	memcpy(rd_data->data, data->gsm_response->data, data->gsm_response->length);

//  data->rdIdx++;
//  data->isBusy = false;
//	
//  return true;
//}

//static inline void circularGsmMessageReset(circular_gsm_response_t* data)    // reset fifo
//{
//  data->wrIdx = data->rdIdx = 0;
//}

// extern ring_buffer_t gps_buffer;
extern ring_buffer_t cellular_ring_buffer;
// extern circular_gsm_response_t circular_gsm_message;

#endif /* __CIRCULAR_H__ */
