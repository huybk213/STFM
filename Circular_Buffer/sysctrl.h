/*
 * Author: HuyTV
 */

#ifndef __SYSCTRL_H__
#define __SYSCTRL_H__

typedef bool (*SC_getDeviceDescriptor)(void);
typedef bool (*SC_power_on)(void);
typedef bool (*SC_power_off)(void);
typedef bool (*SC_power_reset_device)(void);

typedef struct
{
  SC_getDeviceDescriptor      f_getDeviceDescriptor;
  SC_power_on                 f_power_on;
  SC_power_off                f_power_off;
  SC_power_reset_device       f_reset_device;
} sysctrl_funcPtrs_t;

bool SysCtrl_getDeviceDescriptor(void);
bool SysCtrl_power_on(void);		
bool SysCtrl_power_off(void);
bool SysCtrl_reset_device(void);

#endif /* __SYSCTRL_H__ */
