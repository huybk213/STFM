/*
* Author: HuyTV
 */
 
#include "project_config.h"
#include "circular.h"
#include "stm32l4xx_hal.h"
#include "freeRTOS.h"
#include "app_task.h"

extern osThreadId ringBufferTaskId;

volatile uint32_t detect_uart_interrupt_error = 0;
volatile uint32_t detect_ring_buffer_full = 0;
volatile uint8_t just_for_testing[256];
uint16_t j_index = 0;

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)         // UART interrupt callback, do shortly
{
  if (huart->Instance == MODEM_UART_INSTANCE)
  {
			uint8_t tmp_uart_rx =  (uint8_t)MODEM_UART_INSTANCE->RDR;
			if(!ringBufferWrite(&cellular_ring_buffer, tmp_uart_rx))
			{
				// write error, busy is busy or full
				detect_ring_buffer_full++;
			}
			if(ringBufferTaskId) xTaskResumeFromISR(ringBufferTaskId);
			just_for_testing[j_index % 256] = tmp_uart_rx;
			j_index++;
			
  }
	else
	{
				// Should never jump into here
		detect_uart_interrupt_error++;
	}
	uint8_t tmp = 0;
	HAL_UART_Receive_IT(&MODEM_UART_HANDLE, &tmp, 1);
}

volatile bool test_send_sms = false;
void HAL_GPIO_EXTI_Callback (uint16_t GPIO_Pin)
{
    if (GPIO_Pin == GPIO_PIN_13) 
    {
				test_send_sms = true;
		}
}

