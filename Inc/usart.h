/*
 * Author: HuyTV
 */

#ifndef __USART_H__
#define __USART_H__

#include "stm32l4xx_hal.h"
#include "main.h"

#include <stdbool.h>

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;
extern void _Error_Handler(char *, int);

void MX_USART1_UART_Init(void);

bool uartSendByte(UART_HandleTypeDef * huart, uint8_t byte);		// send data to gsm_modem
bool uartSendBuffer(UART_HandleTypeDef * huart, uint8_t* buffer, uint16_t length);

#endif /*__ __USART_H__ */
