#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"
#include "circular.h"
#include "app_task.h"
#include "sysctrl.h"
#include "sysctrl_specific.h"
#include "cellular.h"
#include "app_task.h"
#include "at_core.h"
#include "at_custom.h"

osThreadId defaultTaskHandle;
extern  osThreadId ringBufferTaskId;		// just for testing
void StartDefaultTask(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */


void MX_FREERTOS_Init(void) 
{
  osThreadDef(defaultTask, StartDefaultTask, osPriorityBelowNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);
}

uint8_t  j = 0;
extern  bool test_send_sms;
/* StartDefaultTask function */
void StartDefaultTask(void const * argument)
{
	if(!runScanRingBufferTask(osPriorityNormal, 256)) Error_Handler();
	if(!runProcessReponseMessageTask(osPriorityNormal, 256)) Error_Handler();
	cellularInit();
	atcoreInit(&atcore_context);
  for(;;)
  {
		if(test_send_sms == false)
		{
			osDelay(1);
		}
		else
		{			
					test_send_sms = false;
					at_result_t retval;
					retval = atSendSMS((uint8_t*)"0942018895", (uint8_t*)"hi",  strlen((char*)"hi"), &cellular_context);
					retval++;
					osDelay(1000);
		}

//			at_result_t retval;
//			retval = fsendAtCmdNoParams(&atcore_context , _AT);
//			if(retval == AT_RES_OK)
//			{				
//							retval = fGetMessage(&atcore_context);
//							if( retval == AT_RES_ERROR)
//							{
//								fRspAnalyzeNone(&atcore_context);
//							}
//			}
//		if(!ringBufferWrite(&cellular_ring_buffer, 'A')) Error_Handler();
//		if(!ringBufferWrite(&cellular_ring_buffer, 'T')) Error_Handler();
//		if(!ringBufferWrite(&cellular_ring_buffer, '\r')) Error_Handler();
//		if(!ringBufferWrite(&cellular_ring_buffer, '\r')) Error_Handler();
//		if(!ringBufferWrite(&cellular_ring_buffer, '\n')) Error_Handler();
//		if(!ringBufferWrite(&cellular_ring_buffer, 'O')) Error_Handler();
//		if(!ringBufferWrite(&cellular_ring_buffer, 'K')) Error_Handler();
//		if(!ringBufferWrite(&cellular_ring_buffer, (j++ % 10) + 0x30)) Error_Handler();
//		if(!ringBufferWrite(&cellular_ring_buffer, '\r')) Error_Handler();
//		if(!ringBufferWrite(&cellular_ring_buffer, '\n')) Error_Handler();
//		if(ringBufferTaskId) xTaskResumeFromISR(ringBufferTaskId);
    osDelay(10);
  }
}

