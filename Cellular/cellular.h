/*
 * Author: HuyTV
 */

#ifndef __CELLULAR_H__
#define __CELLULAR_H__

#include "at_core.h"

extern at_context cellular_context;

void cellularInit(void);

void cellularStart(void);

#endif /* __CELLULAR_H__ */
