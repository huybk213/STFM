/*
 * Author: HuyTV
 */

#ifndef __GLOBAL_DEF_H__
#define __GLOBAL_DEF_H__

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#define MSG_BIGGEST_LEN 64

typedef struct
{
    uint8_t msg[MSG_BIGGEST_LEN];
    uint16_t size;
} tPostSimMail_t;

#endif /* __GLOBAL_DEF_H__ */
