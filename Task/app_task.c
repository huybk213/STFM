/*
 * Author: HuyTV
 */

#include "app_task.h"
#include "trace.h"
#include "circular.h"
#include "at_core.h"

osThreadId ringBufferTaskId = NULL;
//osThreadId receiveReponseMessageTaskId = NULL;

tPostSimMail_t sim_mail;
osMailQDef(send_sim_mail_id, 8, tPostSimMail_t);                    // Define mail queue
osMailQId  send_sim_mail_id;


bool runScanRingBufferTask(osPriority taskPrio, uint32_t stackSize)
{
		 LOG("Enter function runScanRingBufferTaskin file %s, line %d\r\n", __FILE__, __LINE__);
		 send_sim_mail_id = osMailCreate(osMailQ(send_sim_mail_id), NULL);  /* create mail queue  */ 
		 if(send_sim_mail_id == NULL)
		 {
			 LOG("osMailCreate creation return NULL pointer\r\n");
			 Error_Handler();
		 }
		 if((tPostSimMail_t*)osMailAlloc(send_sim_mail_id, osWaitForever) == NULL)
		 {
			 LOG("osMailAlloc creation return NULL pointer\r\n");
			 Error_Handler();
		 }
		sim_mail.msg[0] = '\0';
		sim_mail.size = 0;
	
    /* start driver thread */
    osThreadDef(scanRingBufferTask, scanRingBufferTaskBody, taskPrio, 0, stackSize);
    ringBufferTaskId = osThreadCreate(osThread(scanRingBufferTask), NULL);
    if (ringBufferTaskId == NULL)
    {
				LOG("ringBufferTaskId creation return NULL\r\n");
				Error_Handler();
        return false;
    }
    else
    {

    }
		
    return true;
}


bool runProcessReponseMessageTask(osPriority taskPrio, uint32_t stackSize)
{
//		LOG("Enter runProcessReponseMessageTask in file %s, line %d\r\n", __FILE__, __LINE__);
//	  osThreadDef(receiver, processReponseMessageTaskBody, taskPrio, 0, stackSize);
//    receiveReponseMessageTaskId = osThreadCreate(osThread(receiver), NULL);
//	
//    if (receiveReponseMessageTaskId == NULL)
//    {
//        LOG("processReponseMessageTaskId creation error, need more ram size\r\n");
//				Error_Handler();
//        return false;
//    }
//    else
//    {

//    }
		return true;
}

// just for debug
uint8_t begin_scanRingBufferTaskBody = 0;
uint8_t end_scanRingBufferTaskBody = 0;

static void scanRingBufferTaskBody(void const *arg)
{
	LOG("Enter scanRingBufferTaskBody in file %s, line %d\r\n", __FILE__, __LINE__);
	for(;;)
	{
		begin_scanRingBufferTaskBody++;
		if(!ringBufferIsEmpty(&cellular_ring_buffer))
		{	
				uint8_t tmp;
				if(ringBufferRead(&cellular_ring_buffer, &tmp))
				{
						if(cellular_context.at_mode == AT_MODE_CMD)
						{
							switch(cellular_context.crlf_state)
							{
								/* xxx <CR> <CR><LF> uart_message <CR><LF>*/							
								case WAIT_FOR_FIRST_CR:
								{
									/*
									 * Current: 1. xxxx<CR><CR><LF>uart_message<CR><LF>
									 *					or xxxx<CR><LF>uart_message<CR><LF>
									 * We want: <CR><CR><LF>uart_message<CR><LF>
									 * Next state: WAIT_FOR_FIRST_LF
									 */
									if(tmp == '\r')
									{
										cellular_context.crlf_state = WAIT_FOR_FIRST_LF;
									}								
									break;
								}
								
								case WAIT_FOR_FIRST_LF:
								{
									/*
									 * Current: <CR><LF>uart_message<CR><LF>
									 *					or <LF>uart_message<CR><LF>
									 * We want: <LF>uart_message<CR><LF> or <CR><LF>uart_message<CR><LF>
									 * Next state: WAIT_FOR_MSG or WAIT_FOR_FIRST_LF
									 */
									if(tmp == '\n')
									{
										cellular_context.crlf_state = WAIT_FOR_MSG;
									}
									else if(tmp == '\r')
									{
										cellular_context.crlf_state = WAIT_FOR_FIRST_LF;
									}
									break;
								}
								
								case WAIT_FOR_MSG:
								{
									/*
									 * Current: uart_message<CR><LF>
									 * We want: <CR><LF>
									 * Next state: WAIT_FOR_LAST_LF_END
									*/
									if(tmp == '\r')
									{
										cellular_context.crlf_state = WAIT_FOR_LAST_LF_END;
									}
									else
									{
										sim_mail.msg[sim_mail.size++] = tmp;
									}
									break;
								}
								
								case WAIT_FOR_LAST_LF_END:
								{
									if(tmp == '\n')
									{
										// all data come here, implement later
										osStatus retval;
										tPostSimMail_t *mail = (tPostSimMail_t*)osMailAlloc(pool_sim_mail_id, osWaitForever);
										if(mail == NULL)
										{
											Error_Handler();
										}
										else
										{
											memcpy(mail->msg, sim_mail.msg, sim_mail.size);
											mail->size = sim_mail.size;
										}
										
										retval = osMailPut(pool_sim_mail_id, mail);
										if(retval == osOK)
										{
											// reset context
											sim_mail.msg[0] = '\0';		// reset context
											sim_mail.size = 0;
										}
										else
										{
											Error_Handler(); // Maybe mail is full
										}
										
										retval = osMailFree(pool_sim_mail_id, mail);
										if(retval != osOK)
										{
											LOG("osMailFree return error code: %d\r\n", retval);
											Error_Handler();
										}
										
										cellular_context.crlf_state = WAIT_FOR_FIRST_CR;
									}										
									break;
								}
								
								default:	// should never jump into default case 
									break;
							}
						}
						else if(cellular_context.at_mode == AT_MODE_DATA)
						{
										// all data come here, implement later
										switch(cellular_context.crlf_state)
										{
											/* xxx <CR><LF> uart_message*/							
											case WAIT_FOR_FIRST_CR:
											{
												/*
												 * Current: 1. xxxx<<CR><LF>uart_message<CR><LF>
												 * We want: <CR><LF>uart_message
												 * Next state: WAIT_FOR_FIRST_LF
												 */
												if(tmp == '\r')
												{
													cellular_context.crlf_state = WAIT_FOR_FIRST_LF;
												}								
												break;
											}
											
											case WAIT_FOR_FIRST_LF:
											{
												/*
												 * Current: <LF>uart_message<CR><LF>
												 * We want: <LF>uart_message<CR><LF> or <CR><LF>uart_message
												 * Next state: WAIT_FOR_MSG or WAIT_FOR_FIRST_LF
												 */
												if(tmp == '\n')
												{
													cellular_context.crlf_state = WAIT_FOR_MSG;
												}
												else if(tmp == '\r')
												{
													cellular_context.crlf_state = WAIT_FOR_FIRST_LF;
												}
												break;
											}
											
											case WAIT_FOR_MSG:
											{
															osStatus retval;
															sim_mail.msg[sim_mail.size++] = tmp;
															tPostSimMail_t *mail = (tPostSimMail_t*)osMailAlloc(pool_sim_mail_id, osWaitForever);
															if(mail == NULL)
															{
																Error_Handler();
															}
															else
															{
																memcpy(mail->msg, sim_mail.msg, sim_mail.size);
																mail->size = sim_mail.size;
															}
															
															retval = osMailPut(pool_sim_mail_id, mail);
															if(retval == osOK)
															{
																// reset context
																sim_mail.msg[0] = '\0';		// reset context
																sim_mail.size = 0;
															}
															else
															{
																Error_Handler(); // Maybe mail is full
															}
															
//															retval = osMailFree(pool_sim_mail_id, mail);
//															if(retval != osOK)
//															{
//																LOG("osMailFree return error code: %d\r\n", retval);
//																Error_Handler();
//															}
															cellular_context.crlf_state = WAIT_FOR_FIRST_CR;
											}
											
											default: 
												break;
										}
						}
						else if(cellular_context.at_mode == AT_MODE_ECHO)
						{
							sim_mail.msg[sim_mail.size++] = tmp;
							tPostSimMail_t *mail = (tPostSimMail_t*)osMailAlloc(pool_sim_mail_id, osWaitForever);
							if(mail == NULL)
							{
								Error_Handler();
							}
							else
							{
								memcpy(mail->msg, sim_mail.msg, sim_mail.size);
								mail->size = sim_mail.size;
							}
								osStatus retval;
								LOG("Send echo: %d, size %d\r\n", mail->msg[0], 	mail->size);
								retval = osMailPut(pool_sim_mail_id, mail);
								if(retval == osOK)
								{
									// reset context
									memset(sim_mail.msg, 0, sim_mail.size);
									sim_mail.msg[0] = '\0';		// reset context
									sim_mail.size = 0;
								}
								else
								{
									Error_Handler(); // Maybe mail is full
								}
								
//								retval = osMailFree(pool_sim_mail_id, mail);
//								if(retval != osOK)
//								{
//									LOG("osMailFree return error code: %d\r\n", retval);
//									Error_Handler();
//								}
							
						}	
						else
						{
							// should never jump here
						}
						// process uart data herr
				}
				end_scanRingBufferTaskBody++;		
				#if TASK_DEBUG
					if(taskIsLoss(begin_scanRingBufferTaskBody, end_scanRingBufferTaskBody))
					{
						LOG("Task was lost\r\n");
						Error_Handler();
					}
				#endif /* TASK_DEBUG */
		}
		else
		{
			end_scanRingBufferTaskBody++;
			#if TASK_DEBUG
				if(taskIsLoss(begin_scanRingBufferTaskBody, end_scanRingBufferTaskBody))
				{
					LOG("Task was lost\r\n");
					Error_Handler();
				}
			#endif /* TASK_DEBUG */
			vTaskSuspend(NULL);
		}
//		osDelay(1);
	}
}	


// just for debug
uint8_t begin_processReponseMessageTaskBody = 0;
uint8_t end_processReponseMessageTaskBody = 0;

static void processReponseMessageTaskBody(void const *arg)
{
	LOG("Enter processReponseMessageTaskBody in file %s, line %d\r\n", __FILE__, __LINE__);
	osEvent  evt;
	for(;;)
	{
			osDelay(1000);
//		begin_processReponseMessageTaskBody++;
//		evt = osMailGet(pool_sim_mail_id, osWaitForever);  // wait for message
//    if (evt.status == osEventMail) 
//		{
//			tPostSimMail_t *tmp_mail;
//      tmp_mail = (tPostSimMail_t*)evt.value.p;
//			LOG("Got message: ");
//			LOG_PRINTF(tmp_mail->msg, tmp_mail->size);
//			LOG("\r\n");
//			osStatus retval;
//			retval = osMailFree(pool_sim_mail_id, tmp_mail);
//			if(retval != osOK)
//			{
//				LOG("osMailFree return error code: %d\r\n", retval);
//				Error_Handler();
//			}
//			end_processReponseMessageTaskBody++;
//			#if TASK_DEBUG
//			if(taskIsLoss(begin_processReponseMessageTaskBody, end_processReponseMessageTaskBody))
//			{
//				LOG("Task was lost\r\n");
//				Error_Handler();
//			}
//		#endif /* TASK_DEBUG */
//		}
//		else
//		{
//			 LOG("osMailGet returned %02x status\n\r", evt.status);
//			end_processReponseMessageTaskBody++;
//			#if TASK_DEBUG
//			if(taskIsLoss(begin_processReponseMessageTaskBody, end_processReponseMessageTaskBody))
//			{
//				LOG("Task was lost\r\n");
//				Error_Handler();
//			}
//			#endif /* TASK_DEBUG */
//			 vTaskSuspend( NULL );
//		}
	}
}

