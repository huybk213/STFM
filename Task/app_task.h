/*
 * Author: HuyTV
 */

#ifndef __APP_TASK_H__
#define __APP_TASK_H__

#include <stdint.h>
#include <stdbool.h>
#include "cmsis_os.h"

/* this semaphore is used to confirm that a msg has been sent (1 semaphore per device) */
#define mySemaphoreDef(name,index)  \
const osSemaphoreDef_t os_semaphore_def_##name##index = { 0 }

#define mySemaphore(name,index)  \
&os_semaphore_def_##name##index

bool runScanRingBufferTask(osPriority taskPrio, uint32_t stackSize);
bool runProcessReponseMessageTask(osPriority taskPrio, uint32_t stackSize);
void scanRingBufferTask(void const *arg);
static void scanRingBufferTaskBody(void const *arg);
static void processReponseMessageTaskBody(void const *arg);

#endif /* __APP_TASK_H__ */
