/*
 * Author: HuyTV
 */

#include "sysctrl_specific.h"
#include "project_config.h"
#include "trace.h"


static bool SysCtrl_SIM900A_setup(void);

/* Functions Definition ------------------------------------------------------*/
bool SysCtrl_SIM900A_getDeviceDescriptor()
{
  bool retval = false;

  retval = SysCtrl_SIM900A_setup();
	
  return (retval);
}

bool SysCtrl_SIM900A_power_on()
{
  bool retval = true;

  /* Reference: Quectel BG96&EG95&SIM900A&M95 R2.0_Compatible_Design_V1.0
  *  PWRKEY   connected to MODEM_PWR_EN (inverse pulse)
  *  RESET_N  connected to MODEM_RST    (inverse)
  *
  * Turn ON module sequence (cf paragraph 4.2)
  *
  *          PWRKEY  RESET_N  modem_state
  * init       0       0        OFF
  * T=0        1       1        OFF
  * T1=100     0       1        BOOTING
  * T1+100     1       1        BOOTING
  * T1+3500    1       1        RUNNING
  */

  /* Re-enale the UART IRQn */
  HAL_NVIC_EnableIRQ(MODEM_UART_IRQn);

  /* POWER DOWN */
  HAL_GPIO_WritePin(MODEM_RST_GPIO_Port, MODEM_RST_Pin, GPIO_PIN_SET);
  HAL_GPIO_WritePin(MODEM_PWR_EN_GPIO_Port, MODEM_PWR_EN_Pin, GPIO_PIN_SET);
  HAL_Delay(150U);

  /* POWER UP */
  HAL_GPIO_WritePin(MODEM_PWR_EN_GPIO_Port, MODEM_PWR_EN_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(MODEM_RST_GPIO_Port, MODEM_RST_Pin, GPIO_PIN_RESET);
  HAL_Delay(100U);
  HAL_GPIO_WritePin(MODEM_PWR_EN_GPIO_Port, MODEM_PWR_EN_Pin, GPIO_PIN_SET);
  HAL_Delay(200U);
  HAL_GPIO_WritePin(MODEM_PWR_EN_GPIO_Port, MODEM_PWR_EN_Pin, GPIO_PIN_RESET);

  /* Waits for Modem complete its booting procedure: 3.5 s by default*/
  LOG("Waiting 4sec for modem running...");
  HAL_Delay(4000U);
  LOG("...done\r\n");

  return (retval);
}

bool SysCtrl_SIM900A_power_off()
{
  bool retval = true;

  /* Reference: Quectel BG96&EG95&SIM900A&M95 R2.0_Compatible_Design_V1.0
  *  PWRKEY   connected to MODEM_PWR_EN (inverse pulse)
  *  RESET_N  connected to MODEM_RST    (inverse)
  *
  * Turn OFF module sequence (cf paragraph 4.3)
  *
  * Need to use AT+QPOWD command
  * reset GPIO pins to initial state only after completion of previous command (between 2s and 40s)
  */
  HAL_GPIO_WritePin(MODEM_RST_GPIO_Port, MODEM_RST_Pin, GPIO_PIN_SET);
  HAL_GPIO_WritePin(MODEM_PWR_EN_GPIO_Port, MODEM_PWR_EN_Pin, GPIO_PIN_SET);

  return (retval);
}

bool SysCtrl_SIM900A_reset()
{
  bool retval = true;

  /* Reference: Quectel BG96&EG95&SIM900A&M95 R2.0_Compatible_Design_V1.0
  *  PWRKEY   connected to MODEM_PWR_EN (inverse pulse)
  *  RESET_N  connected to MODEM_RST    (inverse)
  *
  * Reset module sequence (cf paragraph 4.4)
  *
  * Can be done using RESET_N pin to low voltage for 100ms minimum
  *
  *          RESET_N  modem_state
  * init       1        RUNNING
  * T=0        0        OFF
  * T=100      1        BOOTING
  * T>=5000    1        RUNNING
  */
  LOG("!!! Hardware Reset triggered !!!\r\n");

  HAL_GPIO_WritePin(MODEM_RST_GPIO_Port, MODEM_RST_Pin, GPIO_PIN_SET);
  HAL_Delay(100U);
  HAL_GPIO_WritePin(MODEM_RST_GPIO_Port, MODEM_RST_Pin, GPIO_PIN_RESET);

  return (retval);
}

static bool SysCtrl_SIM900A_setup(void)
{
  bool retval = true;

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO config */
  HAL_GPIO_WritePin(MODEM_RST_GPIO_Port, MODEM_RST_Pin, GPIO_PIN_SET);
  HAL_GPIO_WritePin(MODEM_PWR_EN_GPIO_Port, MODEM_PWR_EN_Pin, GPIO_PIN_SET);

  GPIO_InitStruct.Pin = MODEM_RST_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(MODEM_RST_GPIO_Port, &GPIO_InitStruct);


  GPIO_InitStruct.Pin = MODEM_PWR_EN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(MODEM_PWR_EN_GPIO_Port, &GPIO_InitStruct);

  /* UART initialization */
  MODEM_UART_HANDLE.Instance = MODEM_UART_INSTANCE;
  MODEM_UART_HANDLE.Init.BaudRate = MODEM_UART_BAUDRATE;
  MODEM_UART_HANDLE.Init.WordLength = MODEM_UART_WORDLENGTH;
  MODEM_UART_HANDLE.Init.StopBits = MODEM_UART_STOPBITS;
  MODEM_UART_HANDLE.Init.Parity = MODEM_UART_PARITY;
  MODEM_UART_HANDLE.Init.Mode = MODEM_UART_MODE;
  MODEM_UART_HANDLE.Init.HwFlowCtl = MODEM_UART_HWFLOWCTRL;
  MODEM_UART_HANDLE.Init.OverSampling = UART_OVERSAMPLING_16;
  MODEM_UART_HANDLE.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;

  if (HAL_UART_Init(&MODEM_UART_HANDLE) != HAL_OK)
  {
    retval = false;
  }
	uint8_t tmp;
	if(HAL_UART_Receive_IT(&MODEM_UART_HANDLE, &tmp, 1) != HAL_OK)
	{
			LOG("HAL_UART_Receive_IT init error\r\n");
			Error_Handler();
	}
	
//  LOG("SIM900A UART config: BaudRate=%d / HW flow ctrl=%d\r\n", MODEM_UART_BAUDRATE, ((MODEM_UART_HWFLOWCTRL == UART_HWCONTROL_NONE) ? 0 : 1));

	retval = SysCtrl_SIM900A_power_on();
  return (retval);
}

