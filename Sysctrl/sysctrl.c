/*
 * Author: HuyTV
 */

#include "sysctrl.h"
#include "sysctrl_specific.h"
#include "project_config.h"
#include "trace.h"

static sysctrl_funcPtrs_t custom_func;

bool atcma_init_sysctrl_func_ptrs(sysctrl_funcPtrs_t *funcPtrs)
{
    bool retval = false;

    LOG("Init SysCtrl func ptrs for device = MODEM SIMCOM SIM900A\r\n");

    /* init function pointers with SIM900A functions */
    funcPtrs->f_getDeviceDescriptor = SysCtrl_SIM900A_getDeviceDescriptor;
    funcPtrs->f_power_on =  SysCtrl_SIM900A_power_on;
    funcPtrs->f_power_off = SysCtrl_SIM900A_power_off;
    funcPtrs->f_reset_device = SysCtrl_SIM900A_reset;

    retval = true;

	return retval;
}

bool SysCtrl_getDeviceDescriptor()
{
  bool retval = false;

  /* check if device is already initialized */
	/* Init SysCtrl functions pointers */
	retval = atcma_init_sysctrl_func_ptrs(&custom_func);
	if (retval == true)
	{

	}
	else
	{
			LOG("SysCtrl_getDeviceDescriptor() error\r\n");
			return false;
	}

  if (retval == true)
  {
    retval = (*custom_func.f_getDeviceDescriptor)();
		if(retval == false) return false;
  }
	
  return (retval);
}


bool SysCtrl_power_on()
{
    bool retval = false;

    retval = (*custom_func.f_power_on)();

    return (retval);
}

bool SysCtrl_power_off()
{
    bool retval = false;

    retval = (*custom_func.f_power_off)();

    return (retval);
}

bool SysCtrl_reset_device()
{
    bool retval = false;

    retval = (*custom_func.f_reset_device)();

    return (retval);
}

