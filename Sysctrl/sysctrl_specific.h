/*
 * Author: HuyTV
 */

#ifndef __SYSCTRL_SPECIFIC_H__
#define __SYSCTRL_SPECIFIC_H__

#include <stdbool.h>

bool SysCtrl_SIM900A_getDeviceDescriptor(void);
bool SysCtrl_SIM900A_power_on(void);
bool SysCtrl_SIM900A_power_off(void);
bool SysCtrl_SIM900A_reset(void);
static bool SysCtrl_SIM900A_setup(void);
static bool SysCtrl_SIM900A_setup(void);

#endif /* __SYSCTRL_SPECIFIC_H__ */
