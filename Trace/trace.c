/*
 * Author: HuyTV
 */

#include "trace.h"
#include "project_config.h"

uint8_t dbg_buf[DBG_MAX_BUFFER_SIZE];

#if ((TRACE_IF_TRACES_ITM == 1U) && (TRACE_IF_TRACES_UART == 1U))
static  uint8_t car[MAX_HEX_PRINT_SIZE];
#else
uint8_t car[MAX_HEX_PRINT_SIZE];
#endif

void traceInitUart(void)
{
		TRACE_UART_GPIO_CLK_ENABLE();
		TRACE_UART_CLK_ENABLE();
	
		GPIO_InitTypeDef GPIO_InitStruct;
    GPIO_InitStruct.Pin = TRACE_UART_GPIO_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = TRACE_UART_GPIO_AF;
    HAL_GPIO_Init(TRACE_UART_GPIO_PORT, &GPIO_InitStruct);

//    /* USART2 interrupt Init */
//    HAL_NVIC_SetPriority(USART2_IRQn, 5, 0);
//    HAL_NVIC_EnableIRQ(USART2_IRQn);
	
	  TRACE_UART_HANDLE.Instance = TRACE_UART_INSTANCE;
		TRACE_UART_HANDLE.Init.BaudRate = TRACE_UART_BAUDRATE;
		TRACE_UART_HANDLE.Init.WordLength = TRACE_UART_WORDLENGTH;
		TRACE_UART_HANDLE.Init.StopBits = TRACE_UART_STOPBITS;
		TRACE_UART_HANDLE.Init.Parity = TRACE_UART_PARITY;
		TRACE_UART_HANDLE.Init.Mode = TRACE_UART_MODE;
		TRACE_UART_HANDLE.Init.HwFlowCtl = TRACE_UART_HWFLOWCTRL;
		TRACE_UART_HANDLE.Init.OverSampling = UART_OVERSAMPLING_16;
		TRACE_UART_HANDLE.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
		TRACE_UART_HANDLE.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
		if (HAL_UART_Init(&TRACE_UART_HANDLE) != HAL_OK)
		{
			_Error_Handler(__FILE__, __LINE__);
		}
}

static void itmOut(uint8_t ch)
{
		ITM_SendChar(ch);
}

void traceItmPrint(uint8_t *ptr, uint16_t len)
{
  uint16_t i;
  for (i = 0U; i < len; i++)
  {
    itmOut(*ptr);
    ptr++;
  }
}

void traceUartPrint(uint8_t *ptr, uint16_t len)
{
  uint32_t i;
  for (i = 0U; i < len; i++)
  {
    HAL_UART_Transmit(&TRACE_INTERFACE_UART_HANDLE, (uint8_t *)ptr, 1U, 0xFFFFU);
    ptr++;
  }
}


void traceHexPrint(uint8_t *buff, uint16_t len)
{
  uint32_t i;

  if ((len * 2U + 1U) > MAX_HEX_PRINT_SIZE)
  {
    LOG("TR (%d/%d)\n\r", (MAX_HEX_PRINT_SIZE >> 1) - 2U, len)
    len = (MAX_HEX_PRINT_SIZE >> 1) - 2U;
  }

  for (i = 0U; i < len; i++)
  {
    uint8_t digit = ((buff[i] >> 4U) & 0xfU);
    if (digit <= 9U)
    {
      car[2U * i] =  digit + 0x30U;
    }
    else
    {
      car[2U * i] =  digit + 0x41U - 10U;
    }

    digit = (0xfU & buff[i]);
    if (digit <= 9U)
    {
      car[2U * i + 1U] =  digit + 0x30U;
    }
    else
    {
      car[2U * i + 1U] =  digit + 0x41U - 10U;
    }
  }
  car[2U * i] =  0U;

  LOG("%s ", (char *)car);
}

void traceBufCharPrint(char *buf, uint16_t size)
{
  for (uint32_t cpt = 0U; cpt < size; cpt++)
  {
    if (buf[cpt] == 0U)
    {
      LOG("<NULL CHAR>");
    }
    else if (buf[cpt] == '\r')
    {
      LOG("<CR>");
    }
    else if (buf[cpt] == '\n')
    {
      LOG("<LF>");
    }
    else if (buf[cpt] == 0x1AU)
    {
      LOG("<CTRL-Z>");
    }
    else if ((buf[cpt] >= 0x20U) && (buf[cpt] <= 0x7EU))
    {
      /* printable char */
      LOG("%c", buf[cpt]);
    }
    else
    {
      /* Special Character - not printable */
      LOG("<SC>");
    }
  }
  LOG("\n\r");
}

void traceIBufHexPrint(char *buf, uint16_t size)
{
  for (uint32_t cpt = 0U; cpt < size; cpt++)
  {
    LOG("0x%02x ", (uint8_t) buf[cpt]);
    if ((cpt != 0U) && ((cpt + 1U) % 16U == 0U))
    {
      LOG("\n\r");
    }
  }
  LOG("\n\r");
}

bool taskIsLoss(uint32_t begin, uint32_t end)		// if task not finished when another task kill, return error	
{
	return (end != begin) ? true : false;
}		

