/*
 * Author: HuyTV
 */

#ifndef __TRACE_H__
#define __TRACE_H__

#include <stdint.h>
#include "project_config.h"

#define DBG_MAX_BUFFER_SIZE 256U
#define MAX_HEX_PRINT_SIZE 210U

extern uint8_t dbg_buf[DBG_MAX_BUFFER_SIZE];

void traceInitUart(void);
void traceItmPrint(uint8_t *ptr, uint16_t len);
void traceUartPrint(uint8_t *ptr, uint16_t len);
void traceHexPrint(uint8_t *buff, uint16_t len);
void traceBufCharPrint(char *buf, uint16_t size);
void traceIBufHexPrint(char *buf, uint16_t size);
bool taskIsLoss(uint32_t begin, uint32_t end);		// if task not finished when another task kill, return error

#if TRACE_ENABLE
		#if ((TRACE_ITM == 1U) && (TRACE_UART == 1U))
						#define LOG(format, args...)    						{ \
																													sprintf((char*)dbg_buf, format "", ## args);\
																													traceItmPrint((uint8_t *)dbg_buf, (uint16_t)strlen((char *)dbg_buf)); \
																													traceUartPrint((uint8_t *)dbg_buf, (uint16_t)strlen((char *)dbg_buf)); \
																												}
	
						#define LOG_PRINTF(buffer, length) 					{ \
																													traceUartPrint((uint8_t *)buffer, length); \
																													traceItmPrint((uint8_t *)buffer, length); \
																												}
																						
																													
		#elif (TRACE_UART == 1)										
						#define LOG(format, args...)   	 	 					{ \
																													sprintf((char*)dbg_buf, format "", ## args);\
																													traceUartPrint((uint8_t *)dbg_buf, (uint16_t)strlen((char *)dbg_buf)); \
																												}
						
						#define LOG_PRINTF(buffer, length) traceUartPrint((uint8_t *)buffer, length)
																							
		
		#elif (TRACE_ITM == 1)
						#define LOG(format, args...)   						{ \
																												sprintf((char*)dbg_buf, format "", ## args);\
																												traceItmPrint((uint8_t *)dbg_buf, (uint16_t)strlen((char *)dbg_buf)); \
																											}
		
						#define LOG_PRINTF(buffer, length) traceItmPrint((uint8_t *)buffer, length)
																							
		#elif (TRACE_PRINTF)
						#define LOG(format, ...)  printf(format, __VA__ARG__)
		#else 
						#define LOG(format, ...)  while(0)																	
		#endif


#define LOG_INVALID_PARAM()					LOG("<%s, %d> ERROR: parameter is null\r\n", __FILE__, __LINE__)
		
#else
																							
	#define LOG(format, ...)  while(0)
	#define LOG_PRINTF(buffer, length) while(0)
																							
#endif /* TRACE_ENABLE */
																							
#endif /* __TRACE_H__ */
